export interface INewUser {
  firstName: string,
  lastName: string,
  email: string,
  age: number,
  friends: string[]
}

export interface IUser {
  id: string,
  data: FirebaseFirestore.DocumentData | undefined
}