export interface ISchema {
  [key: string]: object
}