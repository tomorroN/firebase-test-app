//@ts-ignore
import * as LIVR from 'livr'
//@ts-ignore
import * as extraRules from 'livr-extra-rules'

import { usersSchema } from '../schemas'

export class Validator {
  public execute(input: any, schema: string) {
    const validationRules: object = this._getSchema(schema)

    return this._validateInput(input, validationRules)
  }

  private _getSchema(schema: string): object {
    return usersSchema[schema]
  }

  private _validateInput(input: any, validationRules: object) {
    LIVR.Validator.defaultAutoTrim(true)
    LIVR.Validator.registerDefaultRules(extraRules)

    const validator = new LIVR.Validator(validationRules)

    const validData = validator.validate(input)

    if (validData) return { data: validData }

    return { error: { message: validator.getErrors() } }
  }
}
