import { Router, Request, Response } from 'express'

import { UsersController } from '../controllers'
import { Validator } from '../utils'

export class UsersRouter {
  private readonly _validator: Validator
  private readonly _usersRouter: Router
  private readonly _usersController: UsersController
  private readonly _defaultErrorMessage: string = 'Internal server error. Please contact us or try again later'

  constructor({ usersController, validator }: { usersController: UsersController; validator: Validator }) {
    this._usersRouter = Router()
    this._validator = validator;
    this._usersController = usersController
  }

  public addListener() {
    this._usersRouter
      .post('/', async (req: Request, res: Response) => {
        try {
          const { error, data } = this._validator.execute(req.body, 'body')

          if (error) return res.status(400).json({ error })

          const result = await this._usersController.createUser(data)

          return res.status(201).json(result)
        } catch (error) { console.error(error); return res.status(500).json(this._defaultErrorMessage) }
      })
      .get('/', async (req: Request, res: Response) => {
        try {
          const { error, data } = this._validator.execute(req.query, 'query')

          if (error) return res.status(400).json({ error })

          const result = await this._usersController.getUsers(data)

          return res.json(result)
        } catch (error) { console.error(error); return res.status(500).json(this._defaultErrorMessage) }
      })
      .get('/:id', async (req: Request, res: Response) => {
        try {
          const { error, data } = this._validator.execute(req.params, 'params')

          if (error) return res.status(400).json({ error })

          const result = await this._usersController.getUser(data.id)

          return res.json(result)
        } catch (error) {
          if (error.message.includes('not exist')) return res.status(404).json({ error: { message: error.message } })

          console.error(error)

          return res.status(500).json(this._defaultErrorMessage)
        }
      })
      .put('/:id', async (req: Request, res: Response) => {
        try {
          const { error, data } = this._validator.execute(req.params, 'params')

          if (error) return res.status(400).json({ error })

          const validatedInput = this._validator.execute(req.body, 'body')

          if (validatedInput.error) return res.status(400).json({ error })

          const result = await this._usersController.updateUser(data.id, validatedInput.data);

          return res.json(result)
        } catch (error) { console.error(error); return res.status(500).json(this._defaultErrorMessage) }
      })
      .delete('/:id', async (req: Request, res: Response) => {
        try {
          const { error, data } = this._validator.execute(req.params, 'params')

          if (error) return res.status(400).json({ error })
         
          await this._usersController.deleteUser(data.id)

          return res.sendStatus(204)
        } catch (error) { console.error(error); return res.status(500).json(this._defaultErrorMessage) }
      })

    return this._usersRouter
  }
}
