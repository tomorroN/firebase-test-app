import * as functions from 'firebase-functions'

import * as admin from 'firebase-admin'
import * as express from 'express'
import * as bodyParser from "body-parser"

import { Validator } from './utils'
import { UsersRouter } from './routes'
import { UsersController } from './controllers'

admin.initializeApp(functions.config().firebase)

const db = admin.firestore()
const app = express()
const main = express()

const usersBundle = {
  usersController: new UsersController(db),
  validator: new Validator() 
}

main.use('/api/v1', app)
main.use(bodyParser.json())
main.use(bodyParser.urlencoded({ extended: false }))

app.use('/users', new UsersRouter(usersBundle).addListener())

export const webApi = functions.https.onRequest(main)