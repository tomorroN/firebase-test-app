import { ISchema } from '../interfaces'

export const usersSchema: ISchema = {
  body: {
    firstName: ['required', 'string', { max_length: 200 }, { min_length: 2 }],
    lastName: ['required', 'string', { max_length: 200 }, { min_length: 2 }],
    email: ['required', 'email'],
    age: ['required', 'positive_integer'],
    friends: [{ 'list_of': ['required', 'string'] }]
  },

  query: {
    limit: ['positive_integer'],
    offset: { 'min_number': 0 }
  },

  params: {
    id: ['required', 'string']
  }
}
