import { Firestore } from '@google-cloud/firestore'

import { INewUser, IUser } from '../interfaces'

export class UsersController {
  private readonly _db: Firestore
  private readonly _collection: string = 'users'

  constructor(db: Firestore) {
    this._db = db
  }

  public async createUser(user: INewUser): Promise<IUser> {
    const newUser = await this._db.collection(this._collection).add(user)
    const userDocument = await newUser.get()

    return { id: newUser.id, data: userDocument.data() }
  }

  public async getUser(id: string): Promise<IUser> {
    const user = await this._db.collection(this._collection).doc(id).get()

    if (!user.exists) {
      throw new Error('User does not exist.')
    }

    return { id: user.id, data: user.data() }
  }

  public async getUsers({
    limit, offset
  }: {
    limit?: number; offset?: number
  }): Promise<{ users: IUser[]; total: number }> {
    const usersCollection = this._db.collection(this._collection)
    const usersDocuments = usersCollection.limit(limit || 100).offset(offset || 0)

    const usersQuerySnapshot = await usersDocuments.get()
    const usersCollectionQuerySnapshot = await usersCollection.get()
    
    const total: number = usersCollectionQuerySnapshot.size
    const users: IUser[] = []

    usersQuerySnapshot.forEach((user) => { users.push({ id: user.id, data: user.data() }) })

    return { users, total }
  }

  public async updateUser(id: string, user: INewUser): Promise<IUser> {
    await this._db.collection(this._collection).doc(id).update(user)

    return { id, data: user }
  }

  public async deleteUser(id: string): Promise<void> {
    await this._db.collection(this._collection).doc(id).delete()

    return
  }
}